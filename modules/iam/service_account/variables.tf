
variable "account_id" {
  description = "The account id that is used to generate the service account email address and a stable unique id."
}

variable "display_name" {
  description = "The display name for the service account. Can be updated without creating a new resource."
}

variable "description" {
  description = "A text description of the service account. Must be less than or equal to 256 UTF-8 bytes."
}

variable "project" {
  description = "The ID of the project that the service account will be created in"
}

variable "disabled" {
  description = "Whether a service account is disabled or not."
  default     = false
}