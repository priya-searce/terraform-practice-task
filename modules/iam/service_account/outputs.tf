output "email" {
  value = google_service_account.service_account.email
}

output "name" {
  value = google_service_account.service_account.name
}

output "id" {
  value = google_service_account.service_account.id
}