module "cloud_router" {
  source  = "../../../../../../modules/networking/terraform-google-cloud-router"
  name    = "priya-tfi-cloud-router"
  region  =  var.region
  project = var.project_id
  network = var.network
}
