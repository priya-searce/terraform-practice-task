variable "network" {
  type        = string
  description = "A reference to the network to which this router belongs"
}

variable "project_id" {
  type        = string
  description = "The GCP project ID"
}
variable "region" {
  type        = string
  description = "The GCP region in which this router belongs to"
}
