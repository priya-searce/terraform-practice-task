
output "name" {
  description = "The name for Cloud SQL instance"
  value       = module.priya-test-mysql.instance_name
}

output "replicas" {
  value     = module.priya-test-mysql.replicas
  sensitive = true
}

output "instances" {
  value     = module.priya-test-mysql.instances
  sensitive = true
}
