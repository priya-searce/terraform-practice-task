
output "name" {
  description = "The name for Cloud SQL instance"
  value       = module.priya-test-pgsql.instance_name
}

output "replicas" {
  value     = module.priya-test-pgsql.replicas
  sensitive = true
}

output "instances" {
  value     = module.priya-test-pgsql.instances
  sensitive = true
}
