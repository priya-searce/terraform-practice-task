data "terraform_remote_state" "cloud_router" {
  backend = "gcs"
  config = {
    bucket = "priya-new-terraform-bucket"
    prefix = "terrafrom-improvement/env/dev/regions/asia-south-1/cloud_router/Task_4"
  }
}
