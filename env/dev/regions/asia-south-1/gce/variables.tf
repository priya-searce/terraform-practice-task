/* VM Variables */

#Global variables

variable "project_id" {
  type        = string
  description = "The GCP project ID"
}

# variable "network_project_id" {
#   type        = string
#   description = "It is host network project"
# }

variable "region" {
  type        = string
  description = "Region where the instance template and instance should be created."
}


#Network-Interface

variable "network" {
  description = "The name or self_link of the network to attach this interface to. Use network attribute for Legacy or  subnetted networks and subnetwork for custom subnetted networks."
  default     = ""
}

variable "subnetwork" {
  description = "The name of the subnetwork to attach this interface to. The subnetwork must exist in the same region this instance will be created in. Either network or subnetwork must be provided."
  default     = ""
}

variable "disk_snapshot_policy_name" {
  description = "The name of snapshot policy name for boot and additional disk of GCE instance."
}

# variable "data_disk_info" {
#   description = "The name of snapshot policy name for boot and additional disk of GCE instance."
# }

# variable "disk_labels" {
#   description = "The name of snapshot policy name for boot and additional disk of GCE instance."
# }


# variable "kms_key_self_link" {
#   description = "Key Management service key resource path."
#   default     = ""
# }

#Service Account

variable "service_account" {
  type = object({
    email  = string
    scopes = set(string)
  })
  description = "Service account to attach to the instance"
}

variable "metadata" {
  type        = map(string)
  description = "Metadata, provided as a map"
  default     = {}
}

# variable "gce_ssh_user" {
#     type        = string
#     default     = "abhi"
# }


# variable "gce_ssh_pub_key_file_path" {
#     default     = ""
# }

# variable "ssh_keys" {
#   default = {}
# }

variable "vm-servers" {
  description = "hdfc servers specifications"
}

# variable "instance_image_selflink" {
#   description = "Compute Image to use, e.g. projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20200610. Get it from console REST Response"
#   #default = "projects/searce-playground-v1/global/images/hdfc-es-image"
# }

# Shielded VMs configuration

variable "enable_shielded_vm" {
  default     = true
  description = "Whether to enable the Shielded VM configuration on the instance. Note that the instance image must support Shielded VMs. See https://cloud.google.com/compute/docs/images"
}

variable "shielded_instance_config" {
  description = "Not used unless enable_shielded_vm is true. Shielded VM configuration for the instance."
  type = object({
    enable_secure_boot          = bool
    enable_vtpm                 = bool
    enable_integrity_monitoring = bool
  })

  default = {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }
}

