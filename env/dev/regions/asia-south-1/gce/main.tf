module  "gce_vm_server" {
  source = "../../../../../modules/compute_engine/linux_vm"
  count  = length(var.vm-servers)

  /* global */
  project_id = var.project_id
  region     = var.region

  /* machine details */
  machine_name            = var.vm-servers[count.index]["machine_name"]
  machine_type            = var.vm-servers[count.index]["machine_type"]
  machine_zone            = var.vm-servers[count.index]["machine_zone"]
  instance_labels         = var.vm-servers[count.index]["instance_labels"]
  instance_image_selflink = var.vm-servers[count.index]["instance_image_selflink"]
  vm_deletion_protect     = var.vm-servers[count.index]["vm_deletion_protect"]

  /* network details */
  network            = "projects/${var.project_id}/global/networks/${var.network}"
  subnetwork         = "projects/${var.project_id}/regions/${var.region}/subnetworks/${var.subnetwork}"
  network_tags       = var.vm-servers[count.index]["network_tags"]
  internal_ip        = var.vm-servers[count.index]["internal_ip"]
  enable_external_ip = var.vm-servers[count.index]["enable_external_ip"]

  /* disk details */
  boot_disk_info       = var.vm-servers[count.index]["boot_disk0_info"]
  #disk_labels          = var.vm-servers[count.index]["disk_labels"]
  #data_disk_info       = var.vm-servers[count.index]["data_disk_info"]
  snapshot_policy_name = var.disk_snapshot_policy_name 

  /* service account */
  service_account = var.service_account

  /*metadata*/
  metadata = {
    block-project-ssh-keys = true
    serial-port-logging-enable = false
  }

  shielded_instance_config = var.shielded_instance_config
  
}

# Disk Snapshot Configuration
  
module "disk_policy_creation" {
  source = "../../../../../modules/compute_engine/disk_snapshot_policy"

  /* global */
  policy_name      = var.disk_snapshot_policy_name
  utc_time         = "20:00"
  retention_days   = "30"
  storage_location = "asia"
}

# Additional Disk Configuration

module "attach_data_disk" {
  source               = "../../../../../modules/compute_engine/attach_data_disk"
  count                = length(var.vm-servers)
  compute_instance_id  = module.gce_vm_server[count.index].compute_instance_id
  machine_zone         = module.gce_vm_server[count.index].machine_zone
  data_disk_info       = var.vm-servers[count.index]["data_disk_info"]
  disk_labels          = var.vm-servers[count.index]["disk_labels"] 
  snapshot_policy_name = var.disk_snapshot_policy_name 
}

  
  
  

