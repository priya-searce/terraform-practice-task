/* Output Configurations */

output "gce-server-name" {
  description = "Name of Instance"
  value       = module.gce_vm_server.*.name
}
