#Global
project_id         = "searce-playground-v1"                         
region             = "asia-south1"

#Network Interface
network    =  "saurabh-team-vpc"                                                  
subnetwork =  "mumbai-subnet"                                          

#Snapshot policy
disk_snapshot_policy_name = "my-tf-vm-snapshot-policy"  

#Access scopes 
service_account = {
  email  = "shrutik-poc@searce-playground-v1.iam.gserviceaccount.com"                        #"sa-tradeflow-uat-compute-eng@hbl-uat-tradeflow-prj-spk-4d.iam.gserviceaccount.com"
  scopes = ["cloud-platform"]
}

# VM instance
vm-servers = [
  {
    machine_type = "f1-micro"                   
    machine_name = "my-gce-vm-server"                     
    machine_zone = "asia-south1-a"
    instance_image_selflink ="projects/ubuntu-os-cloud/global/images/ubuntu-2004-focal-arm64-v20220712"
    instance_labels = {
      env   = "tf-task"
      onwer = "priya-soni"
    }
    network_tags = [  
                      "gce-vm-server",
                   ]
    vm_deletion_protect = false
    internal_ip         = "10.1.0.33"
    enable_external_ip  = false
    boot_disk0_info = {
      disk_size_gb = 20       
      disk_type    = "pd-standard"
      auto_delete  = false
      image = "centos-cloud/centos-7"
    }
    data_disk_info = {
      disk_name    = "gce-vm-data-additional-disk" 
      disk_size_gb = 20 #100
      disk_type    = "pd-balanced"
    }
    disk_labels = {
      env   = "allow-iap-access"
    }
  }
]
