module "gce_mig_instance_template" {
  source = "../../../../../../../modules/compute_engine/instance_template"
  name            = var.gce_mig_instance_template["name"]
  project_id      = var.project_id
  machine_type    = var.gce_mig_instance_template["machine_type"]
  tags            = var.gce_mig_instance_template["tags"]
  labels          = var.gce_mig_instance_template["labels"]
  startup_script  = file("scripts/metadata.sh")
  metadata        = var.gce_mig_instance_template["metadata"]
  service_account = var.gce_mig_instance_template["service_account"]
  preemptible     = var.gce_mig_instance_template["preemptible"]

  /* network */
  network        = var.network
  subnetwork     = var.subnetwork
  can_ip_forward = var.gce_mig_instance_template["can_ip_forward"]
  access_config  = var.gce_mig_instance_template["access_config"]

  /* image */
  source_image         = var.gce_mig_instance_template["source_image"]
  source_image_family  = var.gce_mig_instance_template["source_image_family"]
  source_image_project = var.gce_mig_instance_template["source_image_project"]

  /* disks */
  disk_size_gb     = var.gce_mig_instance_template["disk_size_gb"]
  disk_type        = var.gce_mig_instance_template["disk_type"]
  auto_delete      = var.gce_mig_instance_template["auto_delete"]
  additional_disks = var.gce_mig_instance_template["additional_disks"]
}


module "gce_vm_mig" {
  source            = "../../../../../../../modules/compute_engine/mig"
  project_id        = var.project_id
  network           = var.network
  subnetwork        = var.subnetwork
  mig_name          = var.my_vm_mig["mig_name"]
  hostname          = var.my_vm_mig["hostname"]
  region            = var.region
  instance_template = module.gce_mig_instance_template.self_link
  named_ports = var.my_vm_mig["named_ports"]

  /* health check */
  health_check_name = var.my_vm_mig["health_check_name"]
  health_check      = var.my_vm_mig["health_check"]

  /* autoscaler */
  autoscaling_enabled = var.my_vm_mig["autoscaling_enabled"]

  #  target_size         = var.searce_mig["target_size"]
  max_replicas    = var.my_vm_mig["max_replicas"]
  min_replicas    = var.my_vm_mig["min_replicas"]
  cooldown_period = var.my_vm_mig["cooldown_period"]
  autoscaling_cpu = var.my_vm_mig["autoscaling_cpu"]
  update_policy = var.my_vm_mig["update_policy"]
}

