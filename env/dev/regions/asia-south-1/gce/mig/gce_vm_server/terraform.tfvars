/* GLOBAL */
project_id = "searce-playground-v1"
region     = "asia-south1"

/* VPC */
network    =  "saurabh-team-vpc"                                                  
subnetwork =  "mumbai-subnet"                                          


gce_mig_instance_template = {
  name           = "gce-asia-sth1-mig-template"
  machine_type   = "f1-micro"
  tags           = ["allow-iap-access"]
  labels         = { env = "dev", type = "web", app = "searce" }
  metadata       = {}
  #startup_script = "apt-get update; apt-get install -y apache2; "
  preemptible    = true
  service_account = {
    email  = "shrutik-poc@searce-playground-v1.iam.gserviceaccount.com"
    scopes = ["cloud-platform"]
  }
  can_ip_forward       = false
  source_image         = "ubuntu-1804-bionic-v20220712"
  source_image_family  = "ubuntu-1804-lts"
  source_image_project = "ubuntu-os-cloud"

  disk_size_gb     = 20
  disk_type        = "pd-balanced"
  auto_delete      = true
  additional_disks = []

  access_config = []
}


my_vm_mig = {
  mig_name            = "gce-vm-asia-sth1-mig"
  hostname            = "gce-vm-server-asia-sth1-instance"
  autoscaling_enabled = true
  #  target_size         = 1
  min_replicas    = 1
  max_replicas    = 2
  cooldown_period = 60
  autoscaling_cpu = [{ "target" = 0.6 }]

  health_check_name = "gce-vm-server-mig-hc"
  health_check = {
    type                = "http"
    initial_delay_sec   = 120
    check_interval_sec  = 5
    healthy_threshold   = 2
    timeout_sec         = 5
    unhealthy_threshold = 3
    response            = ""
    proxy_header        = "NONE"
    port                = 80
    request             = ""
    request_path        = "/"
    host                = ""
  }

  named_ports = [{
    name = "http"
    port = "80"
  }]
  update_policy = [
    {
      max_surge_fixed              = 4 #maxSurge for regional managed instance group has to be either 0 or at least equal to the number of zones &  maxSurge must be equal to 0 when replacement method is set to RECREATE,
      instance_redistribution_type = "PROACTIVE"
      max_surge_percent            = null
      max_unavailable_fixed        = 0 #maxUnavailable for regional managed instance group has to be either 0 or at least equal to the number of zones & maxUnavailable must be greater than 0 when minimal action is set to REFRESH or RESTART
      max_unavailable_percent      = null
      min_ready_sec                = 30
      minimal_action               = "REPLACE"
      type                         = "PROACTIVE"
      replacement_method           = "SUBSTITUTE"
  }]
}
