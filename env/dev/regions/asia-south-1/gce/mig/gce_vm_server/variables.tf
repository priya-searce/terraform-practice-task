##########
# Common #
##########

variable "region" {
  description = "The GCP region where instances will be deployed."
  type        = string
  default     = "us-central1"
}

variable "network" {
  description = "Network to deploy to. Only one of network or subnetwork should be specified."
  default     = ""
}

variable "subnetwork" {
  description = "Subnet to deploy to. Only one of network or subnetwork should be specified."
  default     = ""
}

variable "project_id" {
  description = "The GCP project to use for integration tests"
  type        = string
}

variable "gce_mig_instance_template" {
  description = "Instance template self_link used to create compute instances"
}

variable "my_vm_mig" {
  description = "Managed instances Group Details"
}
