output "gce_instance_template_name" {
  description = "Name of instance template"
  value       = module.gce_mig_instance_template.*.name
}


output "gce_instance_group" {
  description = "Instance-group url of managed instance group"
  value       = module.gce_vm_mig.*.instance_group
}

