#! /bin/bash
apt-get update
apt-get install -y apache2
cat <<EOF >/var/www/html/index.html
<html><body><h1>HELLO FROM $(hostname)</h1>
</body></html>
EOF
systemctl start apache2
systemctl status apache2