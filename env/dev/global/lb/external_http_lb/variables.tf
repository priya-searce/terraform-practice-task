variable "project" {
  description = "The ID of the project where this VPC will be created"
  type        = string
}

variable "network" {
  description = "The name of the network being created"
  type        = string
}

variable "region" {
  description = "GCP region where the resource will be created"
  type        = string
}


variable "name" {
  description = "name used to generate the LB."
  type        = string
}

