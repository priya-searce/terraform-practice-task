locals {
  health_check = {
    check_interval_sec  = 5
    timeout_sec         = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
    request_path        = "/"
    port                = 80
    host                = null
    logging             = false
  }
}

module "ex-http-lb" {
  source               = "../../../../../modules/lb/external_http_lb"
  name                 = var.name
  project              = var.project
  firewall_networks    = []
  ssl                  = false
  use_ssl_certificates = false
  ssl_certificates     = ["projects/searce-playground-v1/global/sslCertificates/priya-cert"]
  create_address       = true
  #address              = data.terraform_remote_state.web_lb_ip.outputs.web_lb_ip
  create_url_map       = false
  url_map              = google_compute_url_map.urlmap.self_link
  http_forward         = true
  https_redirect       = false

  backends = {
    default = {

      description                     = null
      protocol                        = "HTTP"
      port                            = 80
      port_name                       = "http"
      timeout_sec                     = 30
      connection_draining_timeout_sec = 300
      enable_cdn                      = false
      security_policy                 = null
      session_affinity                = null
      affinity_cookie_ttl_sec         = null
      custom_request_headers          = null
      custom_response_headers         = null

      health_check = local.health_check

      log_config = {
        enable      = false
        sample_rate = 0.0
      }

      groups = [
        {
          group                        = "projects/searce-playground-v1/zones/asia-east1-b/instanceGroups/priya-instance-group"
          balancing_mode               = "UTILIZATION"
          capacity_scaler              = 1.0
          description                  = null
          max_connections              = null
          max_connections_per_instance = null
          max_connections_per_endpoint = null
          max_rate                     = null
          max_rate_per_instance        = null
          max_rate_per_endpoint        = null
          max_utilization              = 1.0
        },
      ]

      iap_config = {
        enable               = false
        oauth2_client_id     = ""
        oauth2_client_secret = ""
      }
    }
    
  }
}



/* URL Mapping */


resource "google_compute_url_map" "urlmap" {
  project         = var.project
  name            = "${var.name}-lb"
  description     = "${var.name} URL Map"
  default_service = module.ex-http-lb.backend_services["default"].self_link

  host_rule {
    hosts        = ["nginx.searceinc.net."]
    path_matcher = "web"
  }

  path_matcher {
    name            = "web"
    default_service = module.ex-http-lb.backend_services["default"].self_link

    path_rule {
      paths   = ["/hello"]
      service = module.ex-http-lb.backend_services["default"].self_link
    }
  }
}

