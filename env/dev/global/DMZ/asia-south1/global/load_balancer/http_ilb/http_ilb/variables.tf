variable "project" {
  description = "The project to deploy to, if not set the default provider project is used."
  type        = string
}
variable "region" {
    description = "region to deploy the load balancer"
    type        = string 
}

variable "network" {
    description = "network  for load balancer"
    type = string 
}

variable "subnetwork" {
    description = "subnetwork for load balancer"
    type = string 
}

variable "name" {
  description = "Name for the forwarding rule and prefix for supporting resources"
  type        = string
}

variable "port_range" {
    description = "forward rule port range"
    type = number 
}