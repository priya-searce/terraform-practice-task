terraform {
  backend "gcs" {
    bucket = "hdfc-wealth-poc-bucket"
    prefix = "terraform/tfstate/networking/load-balancer/http-internal-lb"
  }
}
