/******************************************
	Module for configure Loadbalancer
 *****************************************/

module "gce-lb-http" {
  source           = "../../../../../../../../modules/cloud_loadbalancer/http_ilb"
  name             = var.name
  project          = var.project
  region           = var.region 
  network          = var.network 
  subnetwork       = var.subnetwork 
  port_range       = var.port_range
  
backends = {
    default = {
      description                     = null
      protocol                        = "HTTP"
      port                            = 80
      port_name                       = "http"
      timeout_sec                     = 30
      connection_draining_timeout_sec = 100
      session_affinity                = null
      locality_lb_policy              = "ROUND_ROBIN"
      affinity_cookie_ttl_sec         = null
    
      health_check = {
        check_interval_sec  = 10
        timeout_sec         = 5
        healthy_threshold   = 2
        unhealthy_threshold = 3
        request_path        = "/"
        port                = 80
        host                = null
        logging             = false
      }

      log_config = {
        enable      = false
        sample_rate = null
      }

      groups = [
        {
          group                        = "projects/searce-playground-v1/zones/asia-south1-a/instanceGroups/hdfc-poc-ilb-umig1"
          balancing_mode               = "UTILIZATION"
          capacity_scaler              = 1.0
          description                  = null
          max_connections              = null
          max_connections_per_instance = null
          max_connections_per_endpoint = null
          max_rate                     = null
          max_rate_per_instance        = null
          max_rate_per_endpoint        = null
          max_utilization              = 0.8
        }

      ]
      iap_config = {
        enable               = false
        oauth2_client_id     = ""
        oauth2_client_secret = ""
      }
    }
  }
}
