/******************************************
	GCS Bucket configuration for Terraform State management
 *****************************************/
terraform {
  backend "gcs" {
    bucket = "hdfc-wealth-poc-bucket"
    prefix = "terraform/tfstate/umig/ilb-backend-grp"
  }
}