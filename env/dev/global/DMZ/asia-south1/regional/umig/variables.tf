/******************************************
	Variables for unmanaged instance group
 *****************************************/
variable "project_id" {
  description = "The GCP project ID"
}

variable "region" {
  description = "The GCP region where the unmanaged instance group resides."
}

variable "network" {
  description = "Network name."
}

variable "hdfc_ilb_umig" {
  description = "module for unmanaged instance group"
}

