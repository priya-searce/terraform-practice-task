/******************************************
	Unmanaged insatnce group details
 *****************************************/
output "hdfc_ilb_umig_self_link" {
  description = "Unmanaged instance group name"
  value       = module.hdfc_ilb_umig.unmanaged.self_link
}
