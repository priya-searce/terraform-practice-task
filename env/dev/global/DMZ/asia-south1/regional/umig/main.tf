/******************************************
	Module for configure unmanaged instance group
 *****************************************/

module "hdfc_ilb_umig" {
  source    = "../../../../../../modules/umig/"
  network   = var.network
  project   = var.project_id
  region    = var.region
  name      = var.hdfc_ilb_umig["name"]
  zone      = var.hdfc_ilb_umig["zone"]
  instances = var.hdfc_ilb_umig["instances"]
  port_name = var.hdfc_ilb_umig["port_name"]
  port      = var.hdfc_ilb_umig["port"]

}
