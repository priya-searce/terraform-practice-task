/* Variable's Values */

#Global
project_id         = "searce-playground-v1"
# network_project_id = "hbl-uat-host-prj-hub-4d"
region             = "asia-south1"

#Network Interface
network    = "saurabh-team-vpc"
subnetwork = "mumbai-subnet"

#Snapshot policy
#disk_snapshot_policy_name = "poc-hdfc-gcp-wealthapp-uat-disks-snapshot-policy"

#KMS Key Information
#kms_key_self_link = "projects/hbl-srs-infosec-prj-spk-3c/locations/asia-south1/keyRings/hbl-gcp-app-uat-keyring-as1-kr/cryptoKeys/HBL-TRADEFLOW-UAT-INFRA-KEY"

#User ssh key 
# gce_ssh_user = "<USER_NAME>"
# gce_ssh_pub_key_file_path = "<FILE_PATH>/sample.pub"


#Access scopes 
service_account = {
  email  = "shrutik-poc@searce-playground-v1.iam.gserviceaccount.com"
  scopes = ["cloud-platform"]
}

# VM instance
hdfc-servers = [
  {
    machine_type = "e2-medium"
    machine_name = "poc-hdfc-ilb-backend-server1"
    machine_zone = "asia-south1-a"
    instance_labels = {
      env   = "uat"
      owner = "shrutik"
    }
    network_tags = [  
                      # "allow-443-access",
                      # "allow-devops-access",
                      # "allow-iap-access",
                      # "allow-internal-access"
                      "load-balanced-backend",
                      "ilb-instance"
                   ]
    vm_deletion_protect = false
    # # internal_ip         = "10.1.0.9"
    # internal_ip = ""
    # enable_external_ip = false
    boot_disk0_info = {
      disk_size_gb = 20
      disk_type    = "pd-standard"
      auto_delete  = false
    }
    // data_disk_info = {
    //   disk_name    = "hbl-gcp-tradeflow-uat-bastion-additional-disk"
    //   disk_size_gb = 100
    //   disk_type    = "pd-balanced"
    // }
    disk_labels = {
      env   = "uat"
      instance = "ilb-backend"
    }
  }
]
