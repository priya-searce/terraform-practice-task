/* Backend bucket Configuration to store terraform state files */

terraform {
  backend "gcs" {
    bucket = "hdfc-wealth-poc-bucket"
    prefix = "terraform/tfstate/gce_vm/hdfc-servers/umig-server"
  }
}
