# Disk snapshot policy configuration

# module "disk_policy_creation" {
#   source = "../../../../../../modules/compute_engine/disk_snapshot_policy"
#   policy_name      = var.disk_snapshot_policy_name
#   utc_time         = "20:00"
#   retention_days   = "7"
#   storage_location = "asia"
# }

# Virtual Machine Instance configuration

module "hdfc-servers" {
  source = "../../../../../../../modules/compute_engine/linux_vm"
  count  = length(var.hdfc-servers)

  /* global */
  project_id = var.project_id
  region     = var.region
  # kms_key_self_link = var.kms_key_self_link


  /* machine details */
  machine_name            = var.hdfc-servers[count.index]["machine_name"]
  machine_type            = var.hdfc-servers[count.index]["machine_type"]
  machine_zone            = var.hdfc-servers[count.index]["machine_zone"]
  instance_labels         = var.hdfc-servers[count.index]["instance_labels"]
  vm_deletion_protect     = var.hdfc-servers[count.index]["vm_deletion_protect"]
  instance_image_selflink = "projects/searce-playground-v1/global/images/hdfc-poc-ilb-instance-image"

  /* network details */
  network            = "projects/${var.project_id}/global/networks/${var.network}"
  subnetwork         = "projects/${var.project_id}/regions/${var.region}/subnetworks/${var.subnetwork}"
  network_tags       = var.hdfc-servers[count.index]["network_tags"]
  # internal_ip        = var.hdfc-servers[count.index]["internal_ip"]
  # enable_external_ip = var.hdfc-servers[count.index]["enable_external_ip"]

  
  /* disk details */
  boot_disk_info       = var.hdfc-servers[count.index]["boot_disk0_info"]
  disk_labels          = var.hdfc-servers[count.index]["disk_labels"]
  #snapshot_policy_name = module.disk_policy_creation.policy_name

  /* service account */
  service_account = var.service_account

  /*metadata*/
  metadata = {
    # ssh-keys = "<USER_NAME>:${file("<FILE_PATH>/sample.pub")}"
    
    # startup-script = "${file("scripts/nginx_proxy.sh")}"
  
    block-project-ssh-keys = true
    serial-port-logging-enable = false
  }

  shielded_instance_config = var.shielded_instance_config
  
}

# Additional Disk Configuration

# module "attach_data_disk" {
#   source               = "../../../../../../../modules/compute_engine/attach_data_disk"
#   count                = length(var.hdfc-servers)
#   compute_instance_id  = module.hdfc-servers[count.index].compute_instance_id
#   machine_zone         = module.hdfc-servers[count.index].machine_zone
#   data_disk_info       = var.hdfc-servers[count.index]["data_disk_info"]
#   disk_labels          = var.hdfc-servers[count.index]["disk_labels"]
#   snapshot_policy_name = module.disk_policy_creation.policy_name
#   kms_key_self_link    = var.kms_key_self_link
# }


