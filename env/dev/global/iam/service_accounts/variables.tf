/******************************************
  service_account variables
 *****************************************/

variable "project_id" {
  type        = string
  description = "Variable for Project ID."
}

variable "region" {
  type        = string
  description = "GCP region where the bucket will be created"
}


variable "my_service_accounts" {
  description = "Details of Service Account"
}

variable "my_custom_roles" {
  description = "Custome Roles and Permissions in the Project"
}



