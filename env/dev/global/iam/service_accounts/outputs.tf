# /******************************************
#   Outputs of service_account and role binding for private_service_access
#  *****************************************/


output "service_accounts" {
  value       = module.service_accounts.*.email
  description = "The e-mail address of the service account."
}

output "custom_roles" {
  value       = module.custom_roles.*.custom_role_id
  description = "ID of the custom role created at project level."
}

