module "service_accounts" {
  source = "../../../../../modules/iam/service_account/"
  count  = length(var.my_service_accounts)

  account_id   = var.my_service_accounts[count.index]["account_id"]
  display_name = var.my_service_accounts[count.index]["display_name"]
  description  = var.my_service_accounts[count.index]["description"]
  project      = var.project_id
  disabled     = var.my_service_accounts[count.index]["disabled"]
}

module "custom_roles" {
  source = "../../../../../modules/iam/custom_role_iam/"
  count  = length(var.my_custom_roles)

  target_level         = "project"
  target_id            = var.project_id
  title                = var.my_custom_roles[count.index]["title"]
  role_id              = var.my_custom_roles[count.index]["role_id"]
  base_roles           = var.my_custom_roles[count.index]["base_roles"]
  permissions          = var.my_custom_roles[count.index]["permissions"]
  excluded_permissions = var.my_custom_roles[count.index]["excluded_permissions"]
  description          = var.my_custom_roles[count.index]["description"]
  members              = var.my_custom_roles[count.index]["members"]
}

