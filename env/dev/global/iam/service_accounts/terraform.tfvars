/* GLOBAL */
project_id = "searce-playground-v1"
region     = "asia-south1"

/* Service Account */
my_service_accounts = [
  {
  account_id   = "dev-infra-mgmt-host-gce-sa"
  display_name = "dev-infra-mgmt-host-gce-sa"
  description  = "SA for the infra-mgmt GCE VM"
  disabled     = false
  },
  
  {
  account_id   = "dev-gke-cluster-sa"
  display_name = "dev-gke-cluster-sa"
  description  = "SA for the GKE cluster "
  disabled     = false
  },

  {
  account_id   = "dev-gke-apps-sa"
  display_name = "dev-gke-apps-sa"
  description  = "SA for the GKE applications"
  disabled     = false
  },

  {
  account_id   = "dev-cloud-build-sa"
  display_name = "dev-cloud-build-sa"
  description  = "SA for the Celery applications"
  disabled     = false
  }
]

/* Custom Role */
my_custom_roles = [
  { 
  title      = "prod_infra_mgmt_gce_role"
  role_id    = "ProdInfraMgmtGceRole"
  base_roles = ["roles/compute.admin",
    "roles/compute.networkAdmin",
    "roles/compute.storageAdmin",
    "roles/container.admin",
    "roles/iam.roleAdmin",
    "roles/iam.serviceAccountAdmin",
    "roles/iam.serviceAccountKeyAdmin",
    "roles/iam.serviceAccountUser",
    "roles/resourcemanager.projectIamAdmin",
    "roles/storage.admin"]

  permissions = []
  excluded_permissions = []

  description          = "Custom role for infra mgmt host"
  members              = ["serviceAccount:dev-sa@searce-playground-v1.iam.gserviceaccount.com"]
},

{ 
  title      = "prod_gke_cluster_role"
  role_id    = "ProdGkeClusterRole"
  base_roles = []
  permissions = ["logging.logEntries.create",
    "monitoring.metricDescriptors.create",
    "monitoring.metricDescriptors.get",
    "monitoring.metricDescriptors.list",
    "monitoring.monitoredResourceDescriptors.get",
    "monitoring.monitoredResourceDescriptors.list",
    "monitoring.timeSeries.create",
    "monitoring.timeSeries.list",
    "resourcemanager.projects.get",
    "storage.objects.get",
    "storage.objects.list",
    ]
  excluded_permissions = []
  description          = "Custom role for gke cluster"
  members              = ["serviceAccount:dev-gke-cluster-sa@searce-playground-v1.iam.gserviceaccount.com"]
},

{ 
  title      = "prod_gke_apps_role"
  role_id    = "ProdGkeAppsRole"
  base_roles = []
  permissions = ["storage.buckets.get",
    "storage.buckets.list",
    "storage.buckets.getIamPolicy",
    "storage.buckets.setIamPolicy",
    "storage.buckets.update",
    "storage.objects.create",
    "storage.objects.get",
    "storage.objects.list",
    "resourcemanager.projects.get",
    "secretmanager.versions.access",
    ]
  excluded_permissions = []
  description          = "Custom role for gke apps"
  members              = ["serviceAccount:dev-gke-apps-sa@searce-playground-v1.iam.gserviceaccount.com"]
},

{ 
  title      = "prod_cloud_build_role"
  role_id    = "ProdCloudBuildRole"
  base_roles = ["roles/iam.serviceAccountUser",
    "roles/logging.logWriter",
    "roles/storage.admin",
    "roles/run.admin",
    "roles/secretmanager.secretAccessor",
    "roles/artifactregistry.writer"]

  permissions = []
  excluded_permissions = []
  description          = "Role for Cloud Build"
  members              = ["serviceAccount:dev-cloud-build-sa@searce-playground-v1.iam.gserviceaccount.com"]
}
]


