/******************************************
	IAM user role binding info
 *****************************************/

output "iap_role_members" {
  value = module.iap_role_binding.members
}

/*
output "editor_role_members" {
  value = module.editor_role_binding.members
}
*/
