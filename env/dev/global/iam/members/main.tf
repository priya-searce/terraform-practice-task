/******************************************
  Module for user iap access custom role creation
 *****************************************/

module "custom_role_iap_access" {
  source      = "../../../../../modules/iam/custom_role"
  project     = var.project_id
  role_id     = var.custom_role_iap_access["role_name"]
  permissions = var.custom_role_iap_access["permissions"]
}

/******************************************
	IAM user member IAP Role binding
 *****************************************/

module "iap_role_binding" {
  source   = "../../../../../modules/iam/projects_iam"
  mode     = "additive"
  projects = [var.project_id]

  bindings = {
    "projects/${var.project_id}/roles/${module.custom_role_iap_access.role_name}" = [
      "user:shruti.k@searce.com"
    ]
  }
}

/******************************************
	IAM user member Editor Role binding
 *****************************************/

/*
module "editor_role_binding" {
  source   = "../../../../../modules/iam/projects_iam"
  mode     = "additive"
  projects = [var.project_id]

  bindings = {
    "roles/editor" = [
      "user:client-gcp.user@clientid.com"
    ]
  }
}
*/