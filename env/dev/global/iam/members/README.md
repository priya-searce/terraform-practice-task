# Project IAM Members Module

This module makes it easy to create custom roles and perform role bindings for IAM Members.

- Custom roles
- Perform role bindings for IAM Members.

## Usage
Basic usage of this module is as follows:

* Custom roles

```hcl
module "custom_role" {
  source      = "../../../modules/iam/custom_role/"
  project     = var.project_id
  role_id     = var.custom_role_iap_access["role_name"]
  permissions = var.custom_role_iap_access["permissions"]
}
```

* IAM members and role bindings

```hcl
module "role_binding" {
  source   = "../../../modules/iam/projects_iam"
  mode     = "additive"
  projects = [var.project_id]

  bindings = {
    "projects/${var.project_id}/roles/${module.custom_role.role_name}" = [
      "user1:example-user1@example.com",
      "user2:example-user2@example.com",
    ]
  }
}
```

* Provide the variables values to the modules from terraform.tfvars file.

```hcl
project_id = "example.com"

custom_role_iap_access = {
  role_name       = "example_role"
  permissions = [
    "compute.projects.get",
    "compute.instances.get"
  ]
}
```

* Then perform the following commands in the directory:

   `terraform init` to get the plugins

   `terraform plan` to see the infrastructure plan

   `terraform apply` to apply the infrastructure build

   `terraform destroy` to destroy the built infrastructure